import os
from pathlib import Path

BASE_DIR = Path(__file__).resolve().parent.parent
PROJECT_DIR = BASE_DIR / "example"

SECRET_KEY = 'django-insecure-66#za9zotp07tf*mfeifb-vfu_x+y&b#v==oh58c#6vzn3)c*s'

SITE_ID = 1
DEBUG = True

ALLOWED_HOSTS = []

INSTALLED_APPS = [
    "example.app",
    'simpel_discuss',
    'simpel_themes',
    'simpel_pages',
    'snowpenguin.django.recaptcha2',
    'ckeditor',
    'ckeditor_uploader',
    'filer',
    'easy_thumbnails',
    'django_comments_xtd',
    'django_comments',
    "django.contrib.admin",
    "django.contrib.admindocs",
    "django.contrib.sites",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
]

RECAPTCHA_PUBLIC_KEY = "123"
RECAPTCHA_SECRET_KEY = "12312"

COMMENTS_APP = "django_comments_xtd"
COMMENTS_XTD_MODEL = 'simpel_discuss.models.Threat'
COMMENTS_XTD_FORM_CLASS = 'simpel_discuss.forms.ThreatForm'
COMMENTS_XTD_SALT = b"Timendi causa est nescire. Aequam memento rebus in arduis servare mentem."

# Source mail address used for notifications.
COMMENTS_XTD_FROM_EMAIL = "webmaster@example.com"
COMMENTS_XTD_MAX_THREAD_LEVEL = 1  # default is 0
COMMENTS_XTD_LIST_ORDER = ("-thread_id", "order")  # default is ('thread_id', 'order')

# Contact mail address to show in messages.
COMMENTS_XTD_CONTACT_EMAIL = "helpdesk@example.com"

COMMENTS_XTD_APP_MODEL_OPTIONS = {
    "default": {
        "allow_flagging": True,
        "allow_feedback": True,
        "show_feedback": True,
        "who_can_post": "users",  # Valid values: 'all', users'
    }
}

MIDDLEWARE = [
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "django.middleware.security.SecurityMiddleware",
    "django.middleware.locale.LocaleMiddleware",
]

ROOT_URLCONF = 'example.urls'

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [
            os.path.join(PROJECT_DIR, "templates"),
        ],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "django.template.context_processors.request",
            ],
        },
    },
]

WSGI_APPLICATION = 'example.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BASE_DIR / 'db.sqlite3',
    }
}

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True

LANGUAGE_CODE = "en"
LANGUAGES = [
    ("id", "Indonesia"),
    ("en", "English (United States)"),
]

CKEDITOR_UPLOAD_PATH = 'content/ckeditor/'

STATICFILES_FINDERS = [
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
]

STATICFILES_DIRS = [os.path.join(PROJECT_DIR, "static")]
STATICFILES_STORAGE = "django.contrib.staticfiles.storage.ManifestStaticFilesStorage"

STATIC_ROOT = os.path.join(BASE_DIR, "staticfiles")
STATIC_URL = "/static/"

MEDIA_ROOT = os.path.join(BASE_DIR, "mediafiles")
MEDIA_URL = "/media/"


DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'

SIMPEL_DISCUSS = {
    # Some settings here
    "CAPTCHA_ENABLED": True,
}
